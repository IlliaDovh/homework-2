const joi = require('joi');

module.exports.authValidation = async (req, res, next) => {
  const schema = joi.object({
    username: joi.string().required(),
    password: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.paswordChangingValidation = async (req, res, next) => {
  const schema = joi.object({
    oldPassword: joi.string().required(),
    newPassword: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.noteAddingValidation = async (req, res, next) => {
  const schema = joi.object({
    text: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.noteChangingValidation = async (req, res, next) => {
  const schema = joi.object({
    text: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};
