const express = require('express');

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {noteAddingValidation, noteChangingValidation} =
    require('./middlewares/validationMiddleware');
const {userExistenceChecker, addNote, getNotes, getNote, updateNote,
  changeNoteStatus, deleteNote} = require('../controllers/notesController');

const router = new express.Router();

router.post('/', authMiddleware, asyncWrapper(userExistenceChecker),
    asyncWrapper(noteAddingValidation), addNote);

router.get('/', authMiddleware, asyncWrapper(userExistenceChecker), getNotes);

router.get('/:id', authMiddleware, asyncWrapper(userExistenceChecker),
    getNote);

router.put('/:id', authMiddleware, asyncWrapper(userExistenceChecker),
    asyncWrapper(noteChangingValidation), updateNote);

router.patch('/:id', authMiddleware, asyncWrapper(userExistenceChecker),
    changeNoteStatus);

router.delete('/:id', authMiddleware, asyncWrapper(userExistenceChecker),
    deleteNote);

module.exports = router;
