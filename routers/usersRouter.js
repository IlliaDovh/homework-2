const express = require('express');

const {authMiddleware} = require('./middlewares/authMiddleware');
const {getUserProfileInfo, deleteUserProfile, changeUserPassword} =
    require('../controllers/usersController');
const {asyncWrapper} = require('./helpers');
const {paswordChangingValidation} =
    require('./middlewares/validationMiddleware');

const router = new express.Router();

router.get('/', authMiddleware, getUserProfileInfo);

router.delete('/', authMiddleware, deleteUserProfile);

router.patch('/', authMiddleware, asyncWrapper(paswordChangingValidation),
    changeUserPassword);

module.exports = router;
