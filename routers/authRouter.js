const express = require('express');

const {asyncWrapper} = require('./helpers');
const {authValidation} = require('./middlewares/validationMiddleware');
const {registration, login} = require('../controllers/authController');

const router = new express.Router();

router.post('/register', asyncWrapper(authValidation),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(authValidation), asyncWrapper(login));

module.exports = router;
