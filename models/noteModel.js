const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
    uniq: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
    uniq: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Note = mongoose.model('Note', userSchema);
