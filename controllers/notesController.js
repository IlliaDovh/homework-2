const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

module.exports.userExistenceChecker = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  next();
};

module.exports.addNote = async (req, res, next) => {
  const note = new Note({
    userId: req.user.id,
    text: req.body.text,
  });

  await note.save();
  res.json({message: 'Note created successfully!'});
};

module.exports.getNotes = async (req, res, next) => {
  const {offset, limit} = req.query;
  const notes = await Note.find(
      {userId: req.user.id},
      ['_id', 'userId', 'completed', 'text', 'createdDate'],
      {
        offset: parseInt(offset),
        limit: parseInt(limit),
      },
  );

  res.json({notes: notes});
};

module.exports.getNote = async (req, res, next) => {
  const {id} = req.params;
  const note = await Note.find(
      {userId: req.user.id, _id: id},
      ['_id', 'userId', 'completed', 'text', 'createdDate'],
      {},
  );

  if (!note.length) {
    return res.status(400).json({message: `No note with id ${id} found`});
  }
  res.json({note: note[0]});
};

module.exports.updateNote = async (req, res, next) => {
  const {id} = req.params;
  const note = await Note.findOneAndUpdate({userId: req.user.id, _id: id},
      {text: req.body.text}, {new: true});

  if (!note) {
    return res.status(400).json({message: `No note with id ${id} found`});
  }
  res.json({message: 'Note updated successfully!'});
};

module.exports.changeNoteStatus = async (req, res, next) => {
  const {id} = req.params;
  const note = await Note.findOne({userId: req.user.id, _id: id});

  if (!note) {
    return res.status(400).json({message: `No note with id ${id} found`});
  }
  await note.updateOne({completed: !note.completed});
  res.json({message: 'Note status changed successfully!'});
};

module.exports.deleteNote = async (req, res, next) => {
  const {id} = req.params;
  const note = await Note.findOneAndDelete({userId: req.user.id, _id: id});

  if (!note) {
    return res.status(400).json({message: `No note with id ${id} found`});
  }
  res.json({message: 'Note deleted successfully!'});
};
