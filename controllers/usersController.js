const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

module.exports.getUserProfileInfo = async (req, res, next) => {
  const userProfileInfo =
      await User.findById(req.user.id, {password: 0, __v: 0});

  if (!userProfileInfo) {
    return res.status(400).json({message: 'No user found'});
  }
  res.json({user: userProfileInfo});
};

module.exports.deleteUserProfile = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  await User.findByIdAndDelete(req.user.id);
  await Note.deleteMany({userId: req.user.id});
  res.json({message: 'Success'});
};

module.exports.changeUserPassword = async (req, res, next) => {
  const user = await User.findById(req.user.id);
  const isCorrectPassword =
      user ? await bcrypt.compare(req.body.oldPassword, user.password) : false;

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (!isCorrectPassword || req.body.oldPassword === req.body.newPassword) {
    return res.status(400).json({message: 'Not correct parameters'});
  }
  await User.findByIdAndUpdate(req.user.id, {$set: {
    password: await bcrypt.hash(req.body.newPassword, 10),
  }});
  res.json({message: 'Password changed successfully!'});
};
