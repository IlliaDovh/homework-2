const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
const {JWT_SECRET} = require('../config');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;
  const isUserFound = await User.findOne({username});

  if (isUserFound) {
    return res.status(400)
        .json({message: `Username ${username} already exists`});
  }
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
  res.json({message: 'User registered successfully!'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});
  const isCorrectPassword =
      user ? await bcrypt.compare(password, user.password) : false;

  if (!user || !isCorrectPassword) {
    return res.status(400)
        .json({message: 'No user with provided credentials found'});
  }
  const token = jwt.sign({username: user.username, id: user._id}, JWT_SECRET);

  res.json({message: 'Success!', jwt_token: token});
};
